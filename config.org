#+STARTUP: overview
#+TITLE: My Emacs
#+CREATOR: Laurens Miers
#+LANGUAGE: en
[[./img/dash_logo.png]]

* Installation

My personal emacs configuration

(Heavily) Inspired by the following configs:
    - https://github.com/tuhdo/emacs-c-ide-demo
    - https://github.com/daedreth/UncleDavesEmacs

This configuration requires the installation of :

    - the GNU =global= package (for gtags)
    - =clang= (for ivory)
    - =cmake= (for ivory)
    - =llvm-libs= (for cmake, somehow not a dependency on Manjaro when installing cmake)
    - Use python-pip to install requirements for elpy:
      =pip install jedi flake8 importmagic autopep8 yapf=
    - =ditaa= (for ascii to image generation in org-mode)

When first checking out this config, run =irony-install-server= to make and install the irony-server.

* Resize frame

Minor-mode to easily resize frames (works with EXWM (firefox, ...)).
Courtesy goes to kuanyui (https://gist.github.com/kuanyui/65a408d393871048771c):

#+BEGIN_SRC emacs-lisp
;;; resize-frame.el --- A minor mode to resize frames easily.  -*- lexical-binding: t; -*-

;; Copyright (C) 2014  kuanyui

;; Author: kuanyui <azazabc123@gmail.com>
;; Keywords: frames, tools, convenience
;; License: WTFPL 1.0

;;; Commentary:

;; Press "ESC `" and use arrow-keys or i/j/k/l to adjust frames. press any key to done.

;;; Code:

(defvar resize-frame-map
  (let ((map (make-keymap)))
    (define-key map (kbd "<up>") 'enlarge-window)
    (define-key map (kbd "<down>") 'shrink-window)
    (define-key map (kbd "<right>") 'enlarge-window-horizontally)
    (define-key map (kbd "<left>") 'shrink-window-horizontally)
    (set-char-table-range (nth 1 map) t 'resize-frame-done)
    (define-key map (kbd "C-p") 'enlarge-window)
    (define-key map (kbd "C-n") 'shrink-window)
    (define-key map (kbd "C-f") 'enlarge-window-horizontally)
    (define-key map (kbd "C-b") 'shrink-window-horizontally)
    map))

(define-minor-mode resize-frame
  "A simple minor mode to resize-frame.
C-c C-c to apply."
  ;; The initial value.
  :init-value nil
  ;; The indicator for the mode line.
  :lighter " ResizeFrame"
  ;; The minor mode bindings.
  :keymap resize-frame-map
  :global t
  (if (<= (length (window-list)) 1)
      (progn (setq resize-frame nil)
             (message "Only root frame exists, abort."))
      (message "Use arrow-keys or i/j/k/l to adjust frames.")))

(defun resize-frame-done ()
  (interactive)
  (setq resize-frame nil)
  (message "Done."))

(global-set-key (kbd "C-x C-r") 'resize-frame)
#+END_SRC

* General stuff
** Unsorted

Collection of stuff that needs to be sorted...someday....maybe...
#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "M-p") 'fill-paragraph)
#+END_SRC
** Macro's

Rebind the macro keys to Fx keys to give them a decent purpose.

#+BEGIN_SRC emacs-lisp
(global-set-key [f9]  'start-kbd-macro)
(global-set-key [f10]  'end-kbd-macro)
(global-set-key [f11]  'call-last-kbd-macro)
#+END_SRC

** Goto-line

Starting with Emacs 23.2, =M-g g= is bound to goto-line.
However, I find this too long. So rebind it:

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "M-g") 'goto-line)
#+END_SRC

** Rectangle

Most rectangle functions are by default mapped to something like =C-x r (other-char)=.
I use =string-insert-rectangle= and =query-replace-regexp= quite a lot,
so rebind it to something easy to remember.

#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "C-x r i") 'string-insert-rectangle)
(global-set-key (kbd "C-x r r") 'query-replace-regexp)
#+END_SRC

** Garbage collection (gc)

I used to have the following enabled in my init to increase the gc threshold to speed-up emacs startup:
(stolen from [[http://bling.github.io/blog/2016/01/18/why-are-you-changing-gc-cons-threshold/]])

#+BEGIN_SRC emacs-lisp
(setq gc-cons-threshold 100000000)
#+END_SRC

But according to this: [[https://lists.gnu.org/archive/html/help-gnu-emacs/2007-06/msg00243.html ]],
it is no longer necessary. But I found that I still have to do this to speed up emacs.

** Yes-or-no questions

Because I'm lazy, important yes-or-no questions can be answered with y-or-n:

#+BEGIN_SRC emacs-lisp
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC

** Emacs fullscreen at startup

#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist '(fullscreen . maximized))
#+END_SRC

** Enable disabled commands

Some commands are disabled to protect the user.
Narrow-region/page is a really handy feature, enable it:

#+BEGIN_SRC emacs-lisp
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)
#+END_SRC

** Buffers

Why is this not built-in?

#+BEGIN_SRC emacs-lisp
(defun kill-all-buffers ()
  "Kill all buffers without regard for their origin."
  (interactive)
  (mapc 'kill-buffer (buffer-list)))
#+END_SRC

** Helping vim-users

#+BEGIN_SRC emacs-lisp
(defconst wq "This is not vi!  Use C-x C-c instead.")
(defconst w "This is not vi!  Use C-x C-s instead.")
(defconst q! "This is EMACS not vi!  Use C-x C-c instead.")
(defconst wq! "This is EMACS not vi!  Use C-x C-c instead.")
#+END_SRC

** Backup files

Disable the generation of backup-files, I don't use them.

#+BEGIN_SRC emacs-lisp
(setq make-backup-files nil)
#+END_SRC

** Describe key

Describe key will open a new buffer with the relevant information.
However, it stays in the current window and opens a new window with the help-info, forcing you to switch buffers to close the help window.
This small function just switches the focus to the newly opened window so we can close it more easily.

#+BEGIN_SRC emacs-lisp
(defun move-to-help-window ()
  (switch-to-buffer-other-window "*Help*")
)
(add-hook 'help-mode-hook 'move-to-help-window)
#+END_SRC

* Which-key

Display available keybindings in popup:
https://github.com/justbur/emacs-which-key

#+BEGIN_SRC emacs-lisp
(use-package which-key
  :ensure t
  :config
    (which-key-setup-side-window-bottom)
    (which-key-mode))
#+END_SRC

* Theme

#+BEGIN_SRC emacs-lisp
(use-package monokai-theme
  :ensure t
  :init
    (load-theme 'monokai t)

)
#+END_SRC

** Highlight line

Highlight line will highlight the current line we are on.
Enable highlight-line globally and replace its background colour.

#+BEGIN_SRC emacs-lisp
(global-hl-line-mode 1)
(set-face-background hl-line-face "dark slate grey")
#+END_SRC

* Dashboard

I use the dashboard as start screen.
Since I like it to give me a list of recent files, we need to enable =recentf-mode=.

#+BEGIN_SRC emacs-lisp
(use-package dashboard
  :ensure t
  :init
    (recentf-mode 1)
  :config
    (dashboard-setup-startup-hook)
    (setq dashboard-startup-banner "~/.emacs.d/img/dash_logo.png")
    (setq dashboard-items '((recents  . 10)
                            (bookmarks . 5)
                            ))
    (setq dashboard-banner-logo-title "")
)
#+END_SRC

* Zygospore

Revert =C-x 1= by pressing =C-x 1= again:
[[https://github.com/louiskottmann/zygospore.el]]

FYI: At one point, used this together with sr-speedbar. They did not play well together...

#+BEGIN_SRC emacs-lisp
(use-package zygospore
  :ensure t
  :config
    (global-set-key (kbd "C-x 1") 'zygospore-toggle-delete-other-windows)
)
#+END_SRC

* Mode-line

[[https://github.com/Malabarba/smart-mode-line]]

#+BEGIN_SRC emacs-lisp
(use-package smart-mode-line
  :ensure t
  :config
    (setq sml/no-confirm-load-theme t)
    (setq sml/theme 'respectful)
    (sml/setup)
)
#+END_SRC

** Clock

#+BEGIN_SRC emacs-lisp
  (setq display-time-24hr-format t)
  (setq display-time-format "%H:%M - %d %b %Y")
  (setq display-time-default-load-average nil)

  (display-time-mode 1)
#+END_SRC

* Editing settings

** Kill-ring customization

Setting =kill-whole-line= to non-nil means when we execute =C-k= at the beginning of a line
will the entire line including the following newline will be deleted.

#+BEGIN_SRC emacs-lisp
(setq kill-ring-max 5000) ; increase kill-ring capacity
(setq kill-whole-line t)
#+END_SRC

** Newline at end-of-file

#+BEGIN_SRC emacs-lisp
(setq mode-require-final-newline t) ; add a newline to end of file
#+END_SRC

** Enable column numbers

#+BEGIN_SRC emacs-lisp
(setq column-number-mode 1)
#+END_SRC

** Look-and-feel modifications

Remove scroll-, tool- and menu-bar. I don't use them so free some space.

#+BEGIN_SRC emacs-lisp
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
#+END_SRC

** Tab-width

Set the default tab width.
#+BEGIN_SRC emacs-lisp
(setq-default tab-width 4)
#+END_SRC

** Automatic indent

Automatically indent when pressing =RET=.
#+BEGIN_SRC emacs-lisp
(global-set-key (kbd "RET") 'newline-and-indent)
#+END_SRC

** Delete trailing whitespace

Automatically delete trailing whitespace when saving a file.

#+BEGIN_SRC emacs-lisp
(add-hook 'before-save-hook 'delete-trailing-whitespace)
#+END_SRC

** Angry faces

#+BEGIN_SRC emacs-lisp
;; make angry face to get my attention
(setq prog-modes '(c++-mode python-mode erlang-mode java-mode c-mode emacs-lisp-mode scheme-mode prog-mode))
(make-face 'font-lock-angry-face)
(modify-face 'font-lock-angry-face "Red" "Yellow" nil t nil t nil nil)

;; Add keywords to recognize to angry face
(mapc (lambda (mode)
		(font-lock-add-keywords
		 mode
		 '(("\\<\\(FIXME\\)" 1 'font-lock-angry-face t)))
		)
	  prog-modes)
(mapc (lambda (mode)
		(font-lock-add-keywords
		 mode
		 '(("\\<\\(TODO\\)" 1 'font-lock-angry-face t)))
		)
	  prog-modes)
#+END_SRC

** C Coding settings

Some basic C-coding settings (style, indentation offset, ...).

#+BEGIN_SRC emacs-lisp
;; default coding style
(setq c-default-style "linux")

;; sane indentation offset
(setq c-basic-offset 4)
#+END_SRC

** Tabs vs spaces

Tabs are evil.

#+BEGIN_SRC emacs-lisp
(setq-default indent-tabs-mode nil)
#+END_SRC

* Undo-tree

Undo with =C-/=.
#+BEGIN_SRC emacs-lisp
(use-package undo-tree
  :ensure t
  :config
    (global-undo-tree-mode)
)
#+END_SRC

* Volatile highlights

Show/highlight changes when doing undo/yanks/kills/...

https://github.com/k-talo/volatile-highlights.el

#+BEGIN_SRC emacs-lisp
(use-package volatile-highlights
  :ensure t
  :config
    (volatile-highlights-mode t)
)
#+END_SRC

* iedit

Highlight occurences of symbol and replace them simultanously.
Shortkey: =C-;=

https://github.com/victorhge/iedit

#+BEGIN_SRC emacs-lisp
(use-package iedit
  :ensure t
)
#+END_SRC

* Smartparens

Smart minor-mode to deal with pairs.
Extra options:
    - =show-smartparens-global-mode= : highlight corresponding bracket/pair/...
    - =smartparens-global-mode= : enable smartparens

https://github.com/Fuco1/smartparens

#+BEGIN_SRC emacs-lisp
(use-package smartparens
  :ensure t
  :config
    (require 'smartparens-config)
    (show-smartparens-global-mode t)
    (smartparens-global-mode t)
)

;; old config stuff
;; (setq sp-base-key-bindings 'paredit)
;; (setq sp-autoskip-closing-pair 'always)
;; (setq sp-hybrid-kill-entire-symbol nil)
;; (sp-use-paredit-bindings)
;;
;; (show-smartparens-global-mode +1)
;; (smartparens-global-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; keybinding management smartparens ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; cl-package contains the loop macro
;; (require 'cl)
;;
;; (defmacro def-pairs (pairs)
;;   `(progn
;;      ,@(loop for (key . val) in pairs
;;           collect
;;             `(defun ,(read (concat
;;                             "wrap-with-"
;;                             (prin1-to-string key)
;;                             "s"))
;;                  (&optional arg)
;;                (interactive "p")
;;                (sp-wrap-with-pair ,val)))))
;;
;; (def-pairs ((paren . "(")
;;             (bracket . "[")
;;             (brace . "{")
;;             (single-quote . "'")
;;             (double-quote . "\"")
;;             (underscore . "_")
;;             (back-quote . "`")))
;;
;; (define-key smartparens-mode-map (kbd "C-c (") 'wrap-with-parens)
;; (define-key smartparens-mode-map (kbd "C-c [") 'wrap-with-brackets)
;; (define-key smartparens-mode-map (kbd "C-c {") 'wrap-with-braces)
;; (define-key smartparens-mode-map (kbd "C-c '") 'wrap-with-single-quotes)
;; (define-key smartparens-mode-map (kbd "C-c \"") 'wrap-with-double-quotes)
;; (define-key smartparens-mode-map (kbd "C-c _") 'wrap-with-underscores)
;; (define-key smartparens-mode-map (kbd "C-c `") 'wrap-with-back-quotes)
;;
;; (define-key smartparens-mode-map (kbd "C-c s r") 'sp-rewrap-sexp)
;; (define-key smartparens-mode-map (kbd "C-c s u") 'sp-unwrap-sexp)
;;
;; (define-key smartparens-mode-map (kbd "C-M-f") 'sp-forward-sexp)
;; (define-key smartparens-mode-map (kbd "C-M-b") 'sp-backward-sexp)
;;
;; ;; TODO: in manjaro this selects keyboard-layout or something
;; ;;(define-key smartparens-mode-map (kbd "C-M-k") 'sp-kill-sexp)
;; (define-key smartparens-mode-map (kbd "C-M-w") 'sp-copy-sexp)
;;
;; (define-key smartparens-mode-map (kbd "C-M-n") 'sp-next-sexp)
;; (define-key smartparens-mode-map (kbd "C-M-p") 'sp-previous-sexp)
;;
;; ;; TODO: for some reason this does not work
;; (define-key smartparens-mode-map (kbd "C-M-a") 'sp-beginning-of-sexp)
;; (define-key smartparens-mode-map (kbd "C-M-e") 'sp-end-of-sexp)
;;
;; (define-key smartparens-mode-map (kbd "C-M-h") 'mark-defun)
;;
;; (smartparens-global-mode t)

#+END_SRC

* Comment-dwim-2

Replacement for built-in =comment-dwim=, more comment features.

https://github.com/remyferre/comment-dwim-2

#+BEGIN_SRC emacs-lisp
(use-package comment-dwim-2
  :ensure t
  :config
    (global-set-key (kbd "M-;") 'comment-dwim-2)
)
#+END_SRC

* Expand-region

Expand region increases the selected region by semantic units.
I also enable =pending-delete-mode=, this means when we mark a region and start typing,
the text within the mark is deleted with the new typed text and the mark disappears.

https://github.com/magnars/expand-region.el

#+BEGIN_SRC emacs-lisp
(use-package expand-region
   :ensure t
   :init
     (pending-delete-mode t)
   :config
     (global-set-key (kbd "C-=") 'er/expand-region)
)
#+END_SRC

* Windooze

When we use windows as our bootloader, we have to setup some things first:

#+BEGIN_SRC emacs-lisp
;; Windows performance tweaks
;;
(when (boundp 'w32-pipe-read-delay)
  (setq w32-pipe-read-delay 0))
;; Set the buffer size to 64K on Windows (from the original 4K)
(when (boundp 'w32-pipe-buffer-size)
  (setq irony-server-w32-pipe-buffer-size (* 64 1024)))

;; Set pipe delay to 0 to reduce latency of irony
(setq w32-pipe-read-delay 0)

;; From "setting up irony mode on Windows" :
;; Make sure the path to clang.dll is in emacs' exec_path and shell PATH.
(setenv "PATH"
        (concat
         "C:\\msys64\\usr\\bin" ";"
         "C:\\msys64\\mingw64\\bin" ";"
         (getenv "PATH")
         )
)
(setq exec-path (append '("c:/msys64/usr/bin" "c:/alt/msys64/mingw64/bin")
                        exec-path))
#+END_SRC

To be fair, I didn't test this in a while...

* Projectile

Projectile is a project management tool, full details on:
https://github.com/bbatsov/projectile

#+BEGIN_SRC emacs-lisp
(use-package projectile
  :ensure t
  :config
    (setq projectile-indexing-method 'alien)
    (projectile-global-mode)
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
)
#+END_SRC

* Helm

** General config

#+BEGIN_SRC emacs-lisp
(use-package helm
  :ensure t
  :bind
    ("M-x" . helm-M-x)
    ("M-y" . helm-show-kill-ring)
    ("C-x b" . helm-mini)
    ("C-x C-b" . helm-mini)
    ("C-x C-f" . helm-find-files)
  :init
  (helm-mode 1)
  :config
  (setq helm-M-x-fuzzy-match t
        helm-buffers-fuzzy-matching t
        helm-recentf-fuzzy-match t
        helm-semantic-fuzzy-match t
        helm-imenu-fuzzy-match t
        helm-split-window-inside-p t ;; open helm buffer inside current window
        helm-scroll-amount 8 ;; scroll 8 lines other window using M-<next>/M-<prior>
;;        helm-move-to-line-cycle-in-source nil ;; move to end or beginning of source when reaching to por bottom of source
;;        helm-ff-search-library-in-sexp t ;; search for library in 'require' and 'declare-function' sexp
;;        helm-echo-input-in-header-line t
  )
  ;; rebind tab to do persistent action
  ;; we use helm-execute-persistent-action more than helm-select-action (default for <tab>)
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
  (helm-autoresize-mode 1) ;; Awesome feature together with helm-split-window-inside-p != nil
)

(use-package helm-swoop
  :ensure t
  :bind
    ("C-s" . helm-swoop)
  :config
    ;; "C-s" + "C-s" results in mult-swoop
    (define-key helm-swoop-map (kbd "C-s") 'helm-multi-swoop-all-from-helm-swoop)
    ;; split window inside the current window when multiple windows open
    (setq helm-swoop-split-with-multiple-windows t)
)

;; (require 'helm-config)
;; ;; (define-key helm-find-files-map (kbd "C-b") 'helm-find-files-up-one-level)
;; ;; (define-key helm-find-files-map (kbd "C-f") 'helm-execute-persistent-action)
;;
;; make TAB work in terminal/minibuffer
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)
;; remap helm-select-action: lists actions
(define-key helm-map (kbd "C-z")  'helm-select-action)

;; remap calculator
;; (global-set-key (kbd "C-c C-c") 'helm-calcul-expression)

;; TODO: experiment with mark ring   (breadcrumbs something?)
;; TODO: experiment with helm-regexp (build and test regexes)
;; TODO: remember helm-top (helm interface for top program)

#+END_SRC

** Helm-gtags

#+BEGIN_SRC emacs-lisp
(use-package helm-gtags
  :ensure t
  :bind
    ( "M-." . helm-gtags-find-tag-from-here)
    ( "M-," . helm-gtags-pop-stack)
  :config
    (add-hook 'c-mode-hook 'helm-gtags-mode)
    (add-hook 'c++-mode-hook 'helm-gtags-mode)
    (add-hook 'python-mode-hook 'helm-gtags-mode)
    (add-hook 'java-mode-hook 'helm-gtags-mode)
    (add-hook 'asm-mode-hook 'helm-gtags-mode)

    (custom-set-variables '(helm-gtags-auto-update t))
)
#+END_SRC

** Helm-projectile

Helm interface to projectile:
https://github.com/bbatsov/helm-projectile

#+BEGIN_SRC emacs-lisp
(use-package helm-projectile
  :ensure t
  :init
    (setq helm-projectile-fuzzy-match t)
    (setq projectile-completion-system 'helm)
    (setq projectile-switch-project-action 'helm-projectile)
  :config
    (helm-projectile-on)
)
#+END_SRC

* Mutliple cursors

https://github.com/magnars/multiple-cursors.el

#+BEGIN_SRC emacs-lisp
(use-package multiple-cursors
  :ensure t
  :bind
    ("C-x r a" . mc/edit-lines)
    ("C-x r e" . mc/edit-ends-of-lines)
    ("C->" . mc/mark-next-like-this)
    ("C-<" . mc/mark-previous-like-this)
    ("C-c C->" . mc/mark-all-like-this)
)
#+END_SRC

* GDB

TODO: need to document this

#+BEGIN_SRC emacs-lisp
(setq gdb-many-windows 1)

;; Select a register number which is unlikely to get used elsewere
(defconst egdbe-windows-config-register 313465989
  "Internal used")

(defvar egdbe-windows-config nil)

(defun set-egdbe-windows-config ()
  (interactive)
  (setq egdbe-windows-config (window-configuration-to-register egdbe-windows-config-register)))

(defun egdbe-restore-windows-config ()
  (interactive)
  (jump-to-register egdbe-windows-config-register))

(defun egdbe-start-gdb (&optional gdb-args)
  ""
  (interactive)
  (set-egdbe-windows-config)
  (call-interactively 'gdb))

(defun egdbe-quit ()
  "finish."
  (interactive)
  (gud-basic-call "quit")
  (egdbe-restore-windows-config))

(defun egdbe-gud-mode-hook ()
  ""
  (local-unset-key (kbd "q"))
  (local-set-key (kbd "q") 'egdbe-quit))

(add-hook 'gud-mode-hook 'egdbe-gud-mode-hook)
#+END_SRC

* Magit

#+BEGIN_SRC emacs-lisp
(use-package magit
  :ensure t
  :bind
    ("C-c m" . magit-status)
)
#+END_SRC

* Programming

** Yasnippet

Template system for Emacs.

https://github.com/joaotavora/yasnippet

#+BEGIN_SRC emacs-lisp
(use-package yasnippet
  :ensure t
  :init
    (add-to-list 'load-path
              "~/.emacs.d/plugins/yasnippet")
  :config
    (add-hook 'prog-mode-hook 'yas-minor-mode)
)
#+END_SRC

** Flycheck

On-the-fly syntax checking.

#+BEGIN_SRC emacs-lisp
(use-package flycheck
  :ensure t
  :config
    (add-hook 'prog-mode-hook 'flycheck-mode)
)
#+END_SRC

** Company mode

#+BEGIN_SRC emacs-lisp
(use-package company
  :ensure t
  :config
    (setq company-idle-delay 0)
    (setq company-minimum-prefix-length 2))
    (add-hook 'prog-mode-hook 'company-mode)
#+END_SRC

** (Relative) Line numbers

#+BEGIN_SRC emacs-lisp
(use-package linum-relative
  :ensure t
  :config
    (setq linum-relative-current-symbol "")
    (add-hook 'prog-mode-hook 'linum-relative-mode))
#+END_SRC

** C/C++ mode

*** Flycheck

Clang static analyzer with flycheck

https://github.com/alexmurray/flycheck-clang-analyzer
https://github.com/Sarcasm/flycheck-irony

#+BEGIN_SRC emacs-lisp
(use-package flycheck-clang-analyzer
  :ensure t
  :config
  (with-eval-after-load 'flycheck
    (require 'flycheck-clang-analyzer)
     (flycheck-clang-analyzer-setup)))

(use-package flycheck-irony
  :ensure t
  :config
    (eval-after-load 'flycheck
      '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))
)
#+END_SRC

*** Company

https://github.com/ikirill/irony-eldoc

#+BEGIN_SRC emacs-lisp
(use-package company-c-headers
  :ensure t)

(use-package company-irony
  :ensure t
  :config
  (setq company-backends '((company-c-headers
;;                            company-dabbrev-code ;; not sure what this is
                            company-irony))))

(use-package irony
  :ensure t
  :config
    (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
    (add-hook 'c++-mode-hook 'irony-mode)
    (add-hook 'c-mode-hook 'irony-mode)
    (add-hook 'objc-mode-hook 'irony-mode)
)

(use-package irony-eldoc
  :ensure t
  :config
    (add-hook 'irony-mode-hook 'irony-eldoc)
)
#+END_SRC

** Python mode

Use =elpy=:
https://github.com/jorgenschaefer/elpy

It is a full dev env and sometimes feels like a bit too much but overal good experience.

#+BEGIN_SRC emacs-lisp
(use-package elpy
  :ensure t
  :config
    (elpy-enable)
)
#+END_SRC

* Windows

** Splitting

After you split a window, your focus remains in the previous one.
Credit goes to https://github.com/daedreth/UncleDavesEmacs

#+BEGIN_SRC emacs-lisp
(defun split-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)
#+END_SRC

** Switching

https://github.com/dimitri/switch-window

#+BEGIN_SRC emacs-lisp
(use-package switch-window
  :ensure t
  :config
    (setq switch-window-input-style 'minibuffer)
    (setq switch-window-increase 6)
    (setq switch-window-threshold 2)
    (setq switch-window-shortcut-style 'qwerty)
    (setq switch-window-qwerty-shortcuts
        '("a" "s" "d" "f" "j" "k" "l" "i" "o"))
;;    (setq switch-window-multiple-frames t) ;; TODO: doesn't work properly..
  :bind
    ("C-x o" . switch-window))
#+END_SRC

When using exwm, have a look at this: https://github.com/dimitri/switch-window/pull/62


** Multi-frame rebindings (obsolete with switch-window)

Sometimes I have multiple emacs-frames open.
In the past, I preferred that the normal =C-x o= can deal with this but this is used by switch-window now.

#+BEGIN_SRC emacs-lisp
;; Use C-x o to switch to other frame when using multi-monitor
;; (global-set-key (kbd "C-x o") 'next-multiframe-window)
#+END_SRC

Now that =next-multiframe-window= is bound to =C-x o=,
Bind =C-x p= to =previous-multiframe-window=.

#+BEGIN_SRC emacs-lisp
;; (global-set-key (kbd "\C-x p") 'previous-multiframe-window)
#+END_SRC

* Avy

https://github.com/abo-abo/avy

#+BEGIN_SRC emacs-lisp
(use-package avy
  :ensure t
  :bind
    ("M-s" . avy-goto-char))
#+END_SRC

* Convenience stuff

** Visiting the configuration

#+BEGIN_SRC emacs-lisp
(defun config-visit ()
  (interactive)
  (find-file "~/.emacs.d/config.org"))
(global-set-key (kbd "C-c e") 'config-visit)
#+END_SRC

** Reload the configuration

#+BEGIN_SRC emacs-lisp
(defun config-reload ()
  "Reloads ~/.emacs.d/config.org at runtime"
  (interactive)
  (org-babel-load-file (expand-file-name "~/.emacs.d/config.org")))
(global-set-key (kbd "C-c r") 'config-reload)
#+END_SRC

** Subword

#+BEGIN_SRC emacs-lisp
(global-subword-mode 1)
#+END_SRC

** Bell

The audible bell is annoying AF.

#+BEGIN_SRC emacs-lisp
(setq visible-bell 1)
#+END_SRC

* Server

Emacs as a server.
Emacsclient will then use this emacs as its server.

#+BEGIN_SRC emacs-lisp
(server-start)
#+END_SRC

* Beacon

https://github.com/Malabarba/beacon

#+BEGIN_SRC emacs-lisp
(use-package beacon
  :ensure t
  :config
    (beacon-mode 1)
    (setq beacon-color "#FFFFCC") ;; yelowish
)
#+END_SRC

* Sunrise commander

https://github.com/escherdragon/sunrise-commander

** Install

#+BEGIN_SRC emacs-lisp
(add-to-list 'load-path "~/.emacs.d/extra/sunrise-commander")

(require 'sunrise-commander)
(require 'sunrise-x-buttons)
(require 'sunrise-x-modeline)

(global-set-key (kbd "C-c s") 'sunrise)
(global-set-key (kbd "C-c c") 'sunrise-cd)

(add-to-list 'auto-mode-alist '("\\.srvm\\'" . sr-virtual-mode))
#+END_SRC

* Org

** Org bullets

https://github.com/sabof/org-bullets

#+BEGIN_SRC emacs-lisp
(use-package org-bullets
  :ensure t
  :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))
#+END_SRC

** Some basic config

*** Super/Sub-scripts

Use ={}= for subscripting:

https://orgmode.org/manual/Subscripts-and-superscripts.html

#+BEGIN_SRC emacs-lisp
(setq org-use-sub-superscripts '{})
#+END_SRC

*** Indentation

Preserve indentation in SRC blocks

#+BEGIN_SRC emacs-lisp
(setq org-src-preserve-indentation t)
#+END_SRC


*** Runnable languages

#+BEGIN_SRC emacs-lisp
(org-babel-do-load-languages
 'org-babel-load-languages '(
                             (ditaa . t))
 )
#+END_SRC

**** Dita

Tell org where to look for ditaa

#+BEGIN_SRC emacs-lisp
(setq org-ditaa-jar-path "/usr/share/java/ditaa/ditaa-0_10.jar")
#+END_SRC

** Note config

#+BEGIN_SRC emacs-lisp
;; when ending TODO (C-C C-t) end with a note + timestamp
(setq org-log-done 'note)
;; Add extra states for keywords
(setq org-todo-keywords
      '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))
#+END_SRC

* Diminish

https://github.com/myrjola/diminish.el

#+BEGIN_SRC emacs-lisp
(use-package diminish
  :ensure t
  :config
  (diminish 'volatile-highlights-mode)
  (diminish 'smartparens-mode)
  (diminish 'beacon-mode)
  (diminish 'subword-mode)
  (diminish 'auto-revert-mode)
  (diminish 'helm-mode)
  (diminish 'undo-tree-mode)
  (diminish 'page-break-lines-mode)
)
#+END_SRC

* Shell-pop

https://github.com/kyagi/shell-pop-el

#+BEGIN_SRC emacs-lisp
(use-package shell-pop
  :ensure t
  :bind (("C-c t" . shell-pop))
  :config
  (setq shell-pop-shell-type (quote ("eshell" "*eshell*" (lambda nil (eshell shell-pop-term-shell)))))
  (setq shell-pop-term-shell "/bin/zsh")
  ;; need to do this manually or not picked up by `shell-pop'
  (shell-pop--set-shell-type 'shell-pop-shell-type shell-pop-shell-type))
#+END_SRC

* Old stuff, maybe usefull for lookup later

** Diff mode stuff

#+BEGIN_SRC emacs-lisp
;; show whitespace in diff-mode
;; (add-hook 'diff-mode-hook (lambda ()
;;                             (setq-local whitespace-style
;;                                         '(face
;;                                           tabs
;;                                           tab-mark
;;                                           spaces
;;                                           space-mark
;;                                           trailing
;;                                           indentation::space
;;                                           indentation::tab
;;                                           newline
;;                                           newline-mark))
;;                             (whitespace-mode 1)))
#+END_SRC

** Speedbar

#+BEGIN_SRC emacs-lisp
;; Package: sr-speedbar
;;(require 'sr-speedbar)
;; (add-hook 'emacs-startup-hook (lambda () ; Open sr speedbar on startup
;; 								(sr-speedbar-open)
;; 								))
;; (setq speedbar-show-unknown-files t) ; Enable speedbar to show all files
;; (setq speedbar-use-images nil) ; use text for buttons
;; (setq sr-speedbar-right-side nil) ; put on left side
;; (setq sr-speedbar-width 40)
;;
;; (provide 'setup-speedbar)
#+END_SRC

* Paradox

Paradox is a more modern package menu.
It can update packages async, ...

https://github.com/Malabarba/paradox

#+BEGIN_SRC emacs-lisp
(use-package paradox
  :ensure t
  :init
    (setq paradox-execute-asynchronously t)
  :config
    (paradox-enable)
)
#+END_SRC

* TODO

stuff i need to look into:
- ibuffer
- switch-window
- symon
- spaceline
- async
- exwm
- helm-hide-minibuffer
